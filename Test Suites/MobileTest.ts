<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MobileTest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>167dce6e-f57e-490a-b1a8-a24cfa19f7ba</testSuiteGuid>
   <testCaseLink>
      <guid>abcc563b-89c5-4a9f-9096-18acc9c4a1be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/001_createUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b4318ee-2dfa-42d2-9b5c-037eba795d68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/002_login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>426acbce-8124-4fbb-8bdf-3ed5449298a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/003_addFirstAccount</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
